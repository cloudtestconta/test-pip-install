from fastapi import FastAPI, status, HTTPException
from pydantic import BaseModel
import json

class Item(BaseModel):
    id : int
    marca : str
    preco: int
    cor: str


print("**********")

app = FastAPI()

db =[ {"id":1,
        "marca":"mercedes",
        "preco": 54000,
        "cor": "branco"},
      {"id":2,
        "marca":"tesla",
        "preco": 44000,
        "cor": "branco"},
      {"id":3,
        "marca":"opel",
        "preco": 4000,
        "cor": "preto"}]
    

@app.post("/item/")
def create_item(item:Item):
    db.append(item)
    return item

@app.get("/item/")
def get_items():
    return db

@app.get("/item/{id}")
def get_id(id:int):
    try:
        return db[id]
    except:
        raise HTTPException(status_code=404, detail="Id not found")
